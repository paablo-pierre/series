import React, { Component } from 'react';
import { 
    View, 
    TextInput, 
    StyleSheet, 
    Button, 
    ActivityIndicator, 
    Text,
    Alert,
} from 'react-native';

import firebase from 'firebase';

import FormRow from '../components/FormRow';    

export default class LoginPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            senha: '',
            isLoading: false,
            message: '',
        }
    }

    componentDidMount() {
        
        const config = {
            apiKey: "AIzaSyBiB-_ssKTbhXiaUmsjuZGlt3wsAD0IRcE",
            authDomain: "series-ecd70.firebaseapp.com",
            databaseURL: "https://series-ecd70.firebaseio.com",
            projectId: "series-ecd70",
            storageBucket: "series-ecd70.appspot.com",
            messagingSenderId: "226817451029"
        };
        firebase.initializeApp(config);
    }

    onChangeHandler(field, value) {
        this.setState({
            [field]: value
        });
    }

    tryLogin() {
        this.setState({ isLoading: true, message: '' });
        
        const { email, senha } = this.state;
        
        const loginUserSuccess = user => {
            this.setState({message: 'Sucesso'});   
            this.props.navigation.navigate('Main');         
        } 
        
        const loginUserFailed = error => {
            this.setState({ message: this.getMessageErrorCode(error.code) })
        }

        firebase
            .auth()
            .signInWithEmailAndPassword(email, senha)
            .then(loginUserSuccess)
            .catch(error => {
                if(error.code === 'auth/user-not-found') {
                    Alert.alert(
                        'Usuário não encontrado',
                        'Deseja se cadastrar ?',
                        [{
                            text: 'Não',
                            onPress: () => console.log('Usuario não cadastrado')
                        }, {
                            text: 'Sim',
                            onPress: () => {
                                firebase
                                    .auth()
                                    .createUserWithEmailAndPassword(email, senha)
                                    .then(loginUserSuccess)
                                    .catch(loginUserFailed)
                            }  
                        }],
                        { cancelable: false }                     
                    )
                }else {
                    loginUserFailed
                    console.log('Erro', error);
                }
            })
            .then(() => this.setState({ isLoading: false }));
    }

    getMessageErrorCode(errorCode) {
        switch(errorCode) {
            case 'auth/wrong-password':
                return 'Senha incorreta';
            case 'auth/user-not-found':
                return 'Usuário inválido';
            case 'auth/invalid-email':
                return 'Email inválido';
            case 'auth/email-already-in-use':
                return 'Email já em uso, tente outro';
            case 'auth/invalid-email':
                return 'Email inválido';
            case 'auth/weak-password':
                return 'Senha muito fraca';
            default:
                return 'Error desconhecido';
        }
    }

    renderMessage() {
        const { message } = this.state;
        if(!message)
            return null;
        
        return (
           <View>
               <Text style={{color: 'red'}} > { message } </Text>
           </View>
        );
    }

    renderButton() {
        if(this.state.isLoading)
            return <ActivityIndicator/>;

        return(
            <Button 
                title="Entrar"
                onPress={() => this.tryLogin()} 
            />
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <FormRow first>
                    <TextInput
                        style={styles.input}
                        placeholder="user@email.com"
                        value={this.state.email}
                        onChangeText={value => this.onChangeHandler('email', value)}
                    />
                </FormRow>
                <FormRow last>
                    <TextInput
                        style={styles.input}
                        placeholder="******"
                        value={this.state.senha}
                        onChangeText={value => this.onChangeHandler('senha', value)}                        
                        secureTextEntry={true}
                    />
                </FormRow>
                { this.renderButton() }
                { this.renderMessage() }            
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingLeft: 10,
        paddingRight: 10,
    },
    input: {
        paddingLeft: 5,
        paddingRight: 5,
        paddingBottom: 5,
    }
});